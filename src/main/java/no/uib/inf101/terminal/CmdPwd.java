package no.uib.inf101.terminal;

public class CmdPwd implements Command {
    Context context;
    @Override
    public String run(String[] args) {
        // TODO Auto-generated method stub
        return this.context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "pwd";
    }
    @Override
    public void setContext(Context context) {
        // TODO Auto-generated method stub
        //this.context = new Context();
        this.context = context;
        
    }

    
}
