package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        // TODO Auto-generated method stub
        String ret = "";
        for(String arg : args){
            ret = ret + arg + " ";
        }
        
        return ret;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }
    
}
